from flask import Flask
from flask import request

app = Flask(__name__)

class Calc:
    def add(self, x,y):
        return x+y

    def sub(self, x,y):
        return x-y

@app.route('/')
def index():
    return 'hello bob from user027' 

@app.route('/add', methods=['GET'])
def do_add():
    instance=Calc()
    x=request.args.get('x', type=int)
    y=request.args.get('y', type=int)
    return str(instance.add(x,y))

@app.route('/sub', methods=['GET'])
def sub():
    instance=Calc()
    x=request.args.get('x', type=int)
    y=request.args.get('y', type=int)
    return str(instance.sub(x,y))

if __name__=="__main__":
    app.run(host='0.0.0.0', port=8027) 
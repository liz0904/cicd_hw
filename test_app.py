import unittest
import app

class TestCalc(unittest.TestCase):
    def test_add(self):
        instance=app.Calc()
        result=instance.add(2,3)
        self.assertEqual(result, 5)

    def test_sub(self):
        instance=app.Calc()
        result=instance.sub(2,3)
        self.assertEqual(result, -1)

if __name__=="__main__":
    unittest.main()
